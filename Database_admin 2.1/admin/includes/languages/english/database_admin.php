<?php
/**********************************************************

  file: my_computers.php - v 1.11 2008/08/03

  MY COMPUTERS FILTER CONTRIBUTION
  by Brian Finkelstein ( USKeyser@aol.com )
  http://www.bfafoodservice.com/store

  Please see readme.txt

  for use with osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
**********************************************************/

define('HEADER_TITLE', 'Database Manager');
define('HEADER_TITLE_CURRENT_DB', 'BD actuelle :');
define('HEADER_TITLE_DB_LIST', 'DATABASE LIST');
define('HEADER_TITLE_DROP_CURRENT_DB', 'Drop BD actuelle');
define('HEADER_TITLE_INTERVAL', ' | ');
define('HEADER_TITLE_MENU', 'Menu');
define('HEADER_TITLE_SHOW_DB', 'Show databases');
define('FOOTER_TEXT_DB', 'La base de donn�es : ');
define('FOOTER_TEXT_DB_SERVER', 'Databases on server ');
define('FOOTER_TEXT_DB_SELECTED', 'a �t� s�lectionn�e');
define('FOOTER_TEXT_LOG', 'Log: ');

define('TEXT_ACTIONS', 'Actions');
define('TEXT_ADD_FIELD', 'Add field');
define('TEXT_ADD_INDEX', 'Add index on ');
define('TEXT_AFTER', 'After');
define('TEXT_AFTER_COLUMN', 'After the column ');
define('TEXT_ALTER_TABLE', 'Alter table ');
define('TEXT_ALTERED', ' altered');
define('TEXT_AT_BEGINNING_TABLE', 'At the beginning of the table');
define('TEXT_AT_END_TABLE', 'At the end of the table');
define('TEXT_BACK_TABLE', 'Back to tables ');
define('TEXT_BACK_TABLE_CONTENT', 'Back the contents of the table');
define('TEXT_BROWSE', 'Browse');
define('TEXT_BROWSE_TABLE', 'Browse table');
define('TEXT_CARDINALITY', 'Cardinality');
define('TEXT_CHANGE', 'Change');
define('TEXT_CHECK_QUERY', 'check query');
define('TEXT_CLEAN', 'Clean');
define('TEXT_COLUMNS', 'colums');
define('TEXT_COLUMNS_NUMBERS', 'Colums numbers ');
define('TEXT_CREATE', ' create');
define('TEXT_CREATE_INDEX', 'Create an index in ');
define('TEXT_CREATE_INDEX_2', 'Create index');
define('TEXT_CREATE_INDEX_TABLE', 'Create index on table ');
define('TEXT_CREATE_TABLE', 'CREATE TABLE');
define('TEXT_CREATED', ' created');
define('TEXT_CSV_FILE', 'CSV file: ');
define('TEXT_DB', 'Database ');
define('TEXT_DEFAULT', 'Default');
define('TEXT_DELETE', 'Delete');
define('TEXT_DELETE_RECORD', 'Delete record?');
define('TEXT_DENIED', 'Access denied. Check your settings');
define('TEXT_DROP', 'Drop');
define('TEXT_DROP_DB', 'DROP DATABASE ');
define('TEXT_DROPPED', ' dropped');
define('TEXT_EDIT', 'Edit');
define('TEXT_EDIT_FIELD', 'Edit field ');
define('TEXT_EMPTY', 'Vide');
define('TEXT_EMPTY_NOW', ' now is empty');
define('TEXT_ERROR_CREATING_TABLE', 'Error creating table');
define('TEXT_ERROR_EXECUTING_QUERY', 'Error executing query');
define('TEXT_ERROR_REMOVING_DB', 'Error removing database');
define('TEXT_EXECUTE', 'Execute');
define('TEXT_EXPORT', 'Export');
define('TEXT_EXTRA', 'Extra');
define('TEXT_FAILED', 'FAILED');
define('TEXT_FIELD', 'Field');
define('TEXT_FIRST', 'First');
define('TEXT_FOR_TABLE', ' for table ');
define('TEXT_FROM_TABLE', ' from table ');
define('TEXT_GO', 'Go');
define('TEXT_IMPORT', 'Import');
define('TEXT_IMPORT_CSV', 'Import CSV');
define('TEXT_INDEX', 'Index ');
define('TEXT_INDEX_NAME', 'Name of the index: ');
define('TEXT_INDEX_TYPE', 'Type of index: ');
define('TEXT_INFO', ' a table can have only one primary key is always called PRIMARY');
define('TEXT_INTO_TABLE', ' into table ');
define('TEXT_INSERT', 'Insert');
define('TEXT_KEY_NAME', 'Key name');
define('TEXT_LAST', 'Last');
define('TEXT_NAME', 'Database Administrator - 1 database');
define('TEXT_NULL', 'NULL');
define('TEXT_NUMBER_FIELD', 'Number of fields:');
define('TEXT_OK', 'Ok!');
define('TEXT_OPTIMIZE', 'Optimize');
define('TEXT_PAGE', ' to page ');
define('TEXT_PAGE_PREVIOUS', ' Previous page');
define('TEXT_PAGE_NEXT', ' Next page');
define('TEXT_PRIMARY', 'Primary');
define('TEXT_PRIMARY_KEY', 'Primary key');
define('TEXT_PROPERTIES', 'Properties');
define('TEXT_QUERY', 'QUERY :');
define('TEXT_QUERY_EXECUTE', 'EXecute query');
define('TEXT_QUERY_FROM_FILE', 'QUERY FROM FILE:');
define('TEXT_QUERY_RESULTS', 'Query results :');
define('TEXT_RECORDS_DELETED', 'Deleted records');
define('TEXT_RECORDS_FOR_PAGE', ' records per page');
define('TEXT_RENAME', 'Rename'); 
define('TEXT_RENAME_AS', ' rename as '); 
define('TEXT_RESET', 'Reset');
define('TEXT_SAVE', 'SAVE');
define('TEXT_SELECT', 'Select');
define('TEXT_SELECT_CSV', 'Select <b>CSV</b> file to import into ');
define('TEXT_SELECT_FAILED', 'SELECT FAILED!');
define('TEXT_SELECT_FROM', 'SELECT * FROM');
define('TEXT_SHOW', 'Show');
define('TEXT_SHOW_TABLE', 'Show tables');
define('TEXT_SURE_DROP_FIELD', 'Sure you want to delete the field ');
define('TEXT_SURE_DROP_INDEX', 'Sure you want to delete the index ');
define('TEXT_TABLE', 'Table ');
define('TEXT_TABLE_OF', 'Table of ');
define('TEXT_TABLE_LIST', 'List tables : ');
define('TEXT_TABLE_CREATED', 'created.');
define('TEXT_TABLE_CURRENT', 'Table actuelle :');
define('TEXT_TABLE_INDEXES', 'TABLE INDEXES');
define('TEXT_TABLE_NAME', 'Table name:');
define('TEXT_TABLE_OPERATIONS', 'TABLE OPERATIONS');
define('TEXT_TABLE_OPTIMIZED', 'optimized!');
define('TEXT_TABLE_PROPERTIES', 'Table properties');
define('TEXT_TABLE_RECORDS', 'Enregistrements :');
define('TEXT_TABLE_REMOVED', 'removed!.');
define('TEXT_TABLE_RENAME', 'Rename table to: ');
define('TEXT_TABLE_SELECTED', 'selected.');
define('TEXT_TABLE_STRUCTURE', 'TABLE STRUCTURE');
define('TEXT_TYPE', 'Type');
define('TEXT_UNABLE_ALTERED_TABLE', 'Could not modify the table ');
define('TEXT_UNABLE_CREATE_INDEX', 'Could not create an index ');
define('TEXT_UNABLE_DELETED_RECORDS', 'Can not delete the record');
define('TEXT_UNABLE_DROP_FIELD', 'Could not delete the field ');
define('TEXT_UNABLE_DROP_INDEX', 'Could not delete the index ');
define('TEXT_UNABLE_RENAME_TABLE', 'Could not rename the table ');
define('TEXT_WHERE', 'where');
?>