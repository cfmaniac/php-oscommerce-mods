<?php
/**********************************************************

  file: my_computers.php - v 1.11 2008/08/03

  MY COMPUTERS FILTER CONTRIBUTION
  by Brian Finkelstein ( USKeyser@aol.com )
  http://www.bfafoodservice.com/store

  Please see readme.txt

  for use with osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
**********************************************************/

define('HEADER_TITLE', 'Gestionnaire de base de donn�es');
define('HEADER_TITLE_CURRENT_DB', 'BD actuelle :');
define('HEADER_TITLE_DB_LIST', 'Liste des tables de la BD de :');
define('HEADER_TITLE_DROP_CURRENT_DB', 'Drop BD actuelle');
define('HEADER_TITLE_INTERVAL', ' | ');
define('HEADER_TITLE_MENU', 'BD actuelle');
define('HEADER_TITLE_SHOW_DB', 'Afficher BD');
define('FOOTER_TEXT_DB', 'La base de donn�es : ');
define('FOOTER_TEXT_DB_SERVER', 'Base de donn�es sur le serveur');
define('FOOTER_TEXT_DB_SELECTED', 'a �t� s�lectionn�e');
define('FOOTER_TEXT_LOG', 'Information : ');

define('TEXT_ACTIONS', 'Actions');
define('TEXT_ADD_FIELD', 'Ajouter un champ');
define('TEXT_ADD_INDEX', 'Ajouter un index sur ');
define('TEXT_AFTER', 'Apr�s');
define('TEXT_AFTER_COLUMN', 'Apr�s la colonne ');
define('TEXT_ALTER_TABLE', 'Apr�s la table ');
define('TEXT_ALTERED', ' modifi�');
define('TEXT_AT_BEGINNING_TABLE', 'Au d�but de la table');
define('TEXT_AT_END_TABLE', 'A la fin de la table');
define('TEXT_BACK_TABLE', 'Retour aux tables ');
define('TEXT_BACK_TABLE_CONTENT', 'Retour aux le contenu de la table');
define('TEXT_BROWSE', 'Navigation');
define('TEXT_BROWSE_TABLE', 'Navigation dans la table');
define('TEXT_CARDINALITY', 'Cardinality');
define('TEXT_CHANGE', 'Changer');
define('TEXT_CHECK_QUERY', 'v�rifier requ�te');
define('TEXT_CLEAN', 'Prope');
define('TEXT_COLUMNS', 'colonnes');
define('TEXT_COLUMNS_NUMBERS', 'Nombre de colonnes ');
define('TEXT_CREATE', ' cr�er');
define('TEXT_CREATE_INDEX', 'Cr�er un index dans ');
define('TEXT_CREATE_INDEX_2', 'Cr�er un index');
define('TEXT_CREATE_INDEX_TABLE', 'Cr�er un index sur la table ');
define('TEXT_CREATE_TABLE', 'Cr�er une table :');
define('TEXT_CREATED', ' �tabli');
define('TEXT_CSV_FILE', 'Fichier CSV : ');
define('TEXT_DB', 'La base de donn�es');
define('TEXT_DEFAULT', 'Par d�faut');
define('TEXT_DELETE', 'Supprimer');
define('TEXT_DELETE_RECORD', 'Supprimer l\'enregistrement?');
define('TEXT_DENIED', 'Acc�s refus�. V�rifiez vos param�tres');
define('TEXT_DROP', 'Drop');
define('TEXT_DROP_DB', 'Drop base de donn�es');
define('TEXT_DROPPED', ' chut�');
define('TEXT_EDIT', 'Modifier');
define('TEXT_EDIT_FIELD', 'Modifier champ ');
define('TEXT_EMPTY', 'Vide');
define('TEXT_EMPTY_NOW', ' maintenant est vide');
define('TEXT_ERROR_CREATING_TABLE', 'Erreur de cr�ation du tableau');
define('TEXT_ERROR_EXECUTING_QUERY', 'Erreur ex�cution de la requ�te');
define('TEXT_ERROR_REMOVING_DB', 'Erreur de suppression de base de donn�es');
define('TEXT_EXECUTE', 'Ex�cuter');
define('TEXT_EXPORT', 'Exportation');
define('TEXT_EXTRA', 'Extra');
define('TEXT_FAILED', 'Non ex�cut�');
define('TEXT_FIELD', 'Champ');
define('TEXT_FIRST', 'Premi�re');
define('TEXT_FOR_TABLE', ' pour la table ');
define('TEXT_FROM_TABLE', ' de la table ');
define('TEXT_GO', 'Aller');
define('TEXT_IMPORT', 'Importation');
define('TEXT_IMPORT_CSV', 'Importation CSV');
define('TEXT_INDEX', 'Index ');
define('TEXT_INDEX_NAME', 'Nom de l\'index : ');
define('TEXT_INDEX_TYPE', 'Type d\'index: ');
define('TEXT_INFO', ' une table peut avoir qu\'une seule cl� primaire est toujours appel� PRIMARY');
define('TEXT_INTO_TABLE', ' dans la table ');
define('TEXT_INSERT', 'Ins�rer');
define('TEXT_KEY_NAME', 'Nom de la cl�');
define('TEXT_LAST', 'Dernier');
define('TEXT_NAME', 'Administrateur de base de donn�es - 1 base de donn�es');
define('TEXT_NULL', 'NULL');
define('TEXT_NUMBER_FIELD', 'Nombre de champs :');
define('TEXT_OK', 'Ok!');
define('TEXT_OPTIMIZE', 'Optimisation');
define('TEXT_PAGE', ' � la page ');
define('TEXT_PAGE_PREVIOUS', ' Page pr�c�dente');
define('TEXT_PAGE_NEXT', ' Page suivante');
define('TEXT_PRIMARY', 'Primaire');
define('TEXT_PRIMARY_KEY', 'La cl� primaire');
define('TEXT_PROPERTIES', 'Propri�t�s');
define('TEXT_QUERY', 'Requ�te :');
define('TEXT_QUERY_EXECUTE', 'Ex�cuter la requ�te');
define('TEXT_QUERY_FROM_FILE', 'Requ�te � partir d\'un fichier :');
define('TEXT_QUERY_RESULTS', 'Resultats de la requ�te :');
define('TEXT_RECORDS_DELETED', 'Les enregistrements supprim�s');
define('TEXT_RECORDS_FOR_PAGE', ' enregistrements par page');
define('TEXT_RENAME', 'Renommez'); 
define('TEXT_RENAME_AS', ' renommer comme '); 
define('TEXT_RESET', 'R�initialiser');
define('TEXT_SAVE', 'SAUVER');
define('TEXT_SELECT', 'Select');
define('TEXT_SELECT_CSV', 'S�lectionner un fichier <b>CSV</b> pour importer dans la table ');
define('TEXT_SELECT_FAILED', 'S�lection non ex�cut�e');
define('TEXT_SELECT_FROM', 'Select * from');
define('TEXT_SHOW', 'Montrer');
define('TEXT_SHOW_TABLE', 'Voir les tables');
define('TEXT_SURE_DROP_FIELD', 'Bien s�r, vous voulez supprimer le champ ');
define('TEXT_SURE_DROP_INDEX', 'Bien s�r, vous voulez supprimer l\'index ');
define('TEXT_TABLE', 'La table : ');
define('TEXT_TABLE_OF', 'Tableau d\' ');
define('TEXT_TABLE_LIST', 'Liste des tables : ');
define('TEXT_TABLE_CREATED', 'a �t� cr��e.');
define('TEXT_TABLE_CURRENT', 'Table actuelle :');
define('TEXT_TABLE_INDEXES', 'Indexes de la table');
define('TEXT_TABLE_NAME', 'Nom de la table :');
define('TEXT_TABLE_OPERATIONS', 'Op�ration sur la table');
define('TEXT_TABLE_OPTIMIZED', 'a �t� optimis�e.');
define('TEXT_TABLE_PROPERTIES', 'Propri�t�s de la table');
define('TEXT_TABLE_RECORDS', 'Enregistrements :');
define('TEXT_TABLE_REMOVED', 'a �t� modifi�e.');
define('TEXT_TABLE_RENAME', 'Renommer la table : ');
define('TEXT_TABLE_SELECTED', 'a �t� s�lectionn�e.');
define('TEXT_TABLE_STRUCTURE', 'Structure de la table');
define('TEXT_TYPE', 'Type');
define('TEXT_UNABLE_ALTERED_TABLE', 'Impossible de modifier le table ');
define('TEXT_UNABLE_CREATE_INDEX', 'Impossible de cr�er un index ');
define('TEXT_UNABLE_DELETED_RECORDS', 'Impossible de supprimer l\'enregistrement');
define('TEXT_UNABLE_DROP_FIELD', 'Impossible de supprimer le champ ');
define('TEXT_UNABLE_DROP_INDEX', 'Impossible de supprimer l\'index ');
define('TEXT_UNABLE_RENAME_TABLE', 'Impossible de renommer la table ');
define('TEXT_WHERE', 'where');
?>